package matter.supply.challenge.gist.ui.gist.detail

import android.arch.lifecycle.ViewModel
import matter.supply.challenge.gist.networking.model.Gist

class GistDetailViewModel : ViewModel() {

    private var userGist: Gist? = null

    //region Public
    /**
     * Will return the info of the user's gist.
     *
     * @return [Gist] object selected.
     */
    fun getUserGist(): Gist? = userGist

    /**
     * Will set the info of the user's gist.
     *
     * @param gist set a [Gist] object.
     */
    fun setUserGist(gist: Gist?) {
        userGist = gist
    }
    //endregion
}
