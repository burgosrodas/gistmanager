package matter.supply.challenge.gist.ui.gist.list

import io.reactivex.Observable
import matter.supply.challenge.gist.networking.GistController
import matter.supply.challenge.gist.networking.model.Gist

class GistListPresenter {

    private val gistController: GistController = GistController()

    //region Public
    /**
     * Will return the default Gists list to be displayed.
     *
     * @return an [@link io.reactivex.Observable] for the Github response.
     */
    fun getPublicGistList(): Observable<List<Gist>>? {
        return gistController.getPublicGistList()
    }

    /**
     * Loads from Github servers the public Gist list for a given [userName].
     *
     * @param userName to be searched in github's servers.
     *
     * @return an [@link io.reactivex.Observable] for the Github response.
     */
    fun getPublicUserGistList(userName: String): Observable<List<Gist>>? {
        return gistController.getUserPublicGistList(userName)
    }
    //endregion
}