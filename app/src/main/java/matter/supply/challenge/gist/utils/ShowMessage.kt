package matter.supply.challenge.gist.utils

import android.annotation.SuppressLint
import android.content.Context
import android.support.annotation.ColorRes
import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.text.TextUtils
import android.view.View
import android.widget.TextView
import android.widget.Toast
import matter.supply.challenge.gist.R

/**
 * Class for create different types of messages, like Toast, Dialog or SnackBar.
 *
 * @param builder Builder to configure the message.
 */
class ShowMessage(private val builder: Builder) {

    private fun createDialog(): AlertDialog? {

        val alertBuilder = AlertDialog.Builder(builder.context)

        alertBuilder
            .setTitle(builder.title ?: builder.context.getString(R.string.app_name))
            .setMessage(builder.message)
            .setCancelable(builder.isCancelable)
            .setOnCancelListener { builder.action?.onButtonClicked(ButtonType.NONE) }
            .setPositiveButton(
                builder.positiveMessage ?: builder.context.getString(android.R.string.yes)
            ) { _, _ -> builder.action?.onButtonClicked(ButtonType.POSITIVE) }

        if (!TextUtils.isEmpty(builder.negativeMessage)) {
            alertBuilder.setNegativeButton(builder.negativeMessage) { _, _ ->
                builder.action?.onButtonClicked(ButtonType.NEGATIVE)
            }
        }

        if (!TextUtils.isEmpty(builder.neutralMessage)) {
            alertBuilder.setNeutralButton(builder.neutralMessage) { _, _ ->
                builder.action?.onButtonClicked(ButtonType.NEUTRAL)
            }
        }

        return alertBuilder.create()
    }

    private fun createSnackBar(): Snackbar? {
        val snackBar = Snackbar.make(builder.rootView, builder.message ?: "", Snackbar.LENGTH_LONG)

        if (builder.backgroundColor != 0) {
            snackBar.view
                .setBackgroundColor(ContextCompat.getColor(builder.context, builder.backgroundColor))
        }

        if (builder.textColor != 0) {
            val textView = snackBar.view.findViewById<TextView>(android.support.design.R.id.snackbar_text)

            textView?.setTextColor(ContextCompat.getColor(builder.context, builder.textColor))
            snackBar.setActionTextColor(ContextCompat.getColor(builder.context, builder.textColor))
        }

        builder.action?.let {
            snackBar.setAction(builder.positiveMessage) { _ ->
                builder.action?.onButtonClicked(ButtonType.POSITIVE)
            }
        }

        return snackBar
    }

    @SuppressLint("ShowToast")
    private fun createToast(): Toast? {

        val toast = Toast.makeText(builder.context, builder.message, Toast.LENGTH_LONG)

        if (builder.backgroundColor != 0) {
            toast.view.setBackgroundColor(
                ContextCompat.getColor(builder.context, builder.backgroundColor)
            )
        }

        if (builder.textColor != 0) {

            val text = toast.view.findViewById<TextView>(android.R.id.message)

            text?.let {
                it.setShadowLayer(0f, 0f, 0f, 0)
                it.setTextColor(ContextCompat.getColor(builder.context, builder.textColor))
            }
        }

        return toast
    }

    open class Builder(val context: Context) {
        lateinit var rootView: View
        private var dialogType = DialogType.DIALOG
        var title: String? = null
        var message: String? = null
        var positiveMessage: String? = null
        var negativeMessage: String? = null
        var neutralMessage: String? = null
        var isCancelable = false
        var action: DialogConfirmInterface? = null
        @ColorRes
        var textColor: Int = 0
        @ColorRes
        var backgroundColor: Int = 0

        constructor(rootView: View) : this(rootView.context) {
            this.rootView = rootView
        }

        fun setTitle(@StringRes titleRes: Int): Builder {
            this.title = context.getString(titleRes)
            return this
        }

        fun setMessage(message: String): Builder {

            if (TextUtils.isEmpty(message)) {
                throw IllegalStateException("Not valid message for ShowMessage.setMessage(String)")
            }

            this.message = message
            return this
        }

        fun setMessage(@StringRes messageRes: Int): Builder {
            setMessage(context.getString(messageRes))
            return this
        }

        fun setDialogType(dialogType: DialogType): Builder {
            this.dialogType = dialogType
            return this
        }

        fun setPositive(@StringRes positiveRes: Int): Builder {
            this.positiveMessage = context.getString(positiveRes)
            return this
        }

        fun setNegative(@StringRes negativeRes: Int): Builder {
            this.negativeMessage = context.getString(negativeRes)
            return this
        }

        fun setTitle(title: String): Builder {
            this.title = title
            return this
        }

        fun setCancelable(isCancelable: Boolean): Builder {
            this.isCancelable = isCancelable
            return this
        }

        fun setAction(action: DialogConfirmInterface): Builder {
            this.action = action
            return this
        }

        fun show(): Any? {

            var result: Any? = null
            val dialogMessage = ShowMessage(this)

            when (dialogType) {
                DialogType.DIALOG -> {
                    result = dialogMessage.createDialog()
                    result?.show()
                }
                DialogType.SNACK_BAR -> {
                    result = dialogMessage.createSnackBar()
                    result?.show()
                }
                DialogType.TOAST -> {
                    result = dialogMessage.createToast()
                    result?.show()
                }
            }

            return result
        }
    }

    enum class DialogType {
        DIALOG, SNACK_BAR, TOAST
    }

    enum class ButtonType {
        POSITIVE, NEGATIVE, NEUTRAL, NONE
    }

    interface DialogConfirmInterface {
        fun onButtonClicked(buttonClicked: ButtonType)
    }
}