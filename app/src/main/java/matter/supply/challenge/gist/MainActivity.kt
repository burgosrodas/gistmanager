package matter.supply.challenge.gist

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import matter.supply.challenge.gist.base.BaseActivity
import matter.supply.challenge.gist.base.BaseFragment
import matter.supply.challenge.gist.ui.gist.list.GistListFragment

class MainActivity : BaseActivity() {

    companion object {
        const val TAG = "MainActivity"
    }

    private var searchView: SearchView? = null
    private val fragmentList = GistListFragment.newInstance()

    //region Override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        if (savedInstanceState == null) {
            setFragment(fragmentList)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)
        initSearch(menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_search -> return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (searchView?.isIconified == false) {
            searchView?.isIconified = true
            return
        }
        super.onBackPressed()
    }
    //endregion

    //region Private
    private fun initSearch(menu: Menu?) {
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as? SearchManager
        searchView = menu?.findItem(R.id.action_search)?.actionView as? SearchView
        searchView?.apply {
            setSearchableInfo(searchManager?.getSearchableInfo(componentName))
            maxWidth = Integer.MAX_VALUE
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    fragmentList.searchUser(query!!)
                    return false
                }

                override fun onQueryTextChange(query: String?): Boolean {
                    return false
                }
            })

            setOnSearchClickListener { setBackArrow(true) }
            setOnCloseListener {
                setBackArrow(false)
                fragmentList.loadInitData()
                false
            }
        }
    }

    private fun setFragment(fragment: BaseFragment) {
        try {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .commitNow()
        } catch (e: Exception) {
            Log.e(TAG, "", e)
        }
    }
    //endregion

}
