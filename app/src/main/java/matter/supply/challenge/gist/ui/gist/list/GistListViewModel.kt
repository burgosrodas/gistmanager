package matter.supply.challenge.gist.ui.gist.list

import android.arch.lifecycle.ViewModel

class GistListViewModel : ViewModel() {

    private val presenter: GistListPresenter = GistListPresenter()

    //region Public
    /**
     * Will return the presenter to handle the Github info.
     *
     * @return presenter
     */
    fun getPresenter(): GistListPresenter = presenter
    //endregion
}
