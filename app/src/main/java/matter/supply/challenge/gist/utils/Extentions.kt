package matter.supply.challenge.gist.utils

import android.graphics.drawable.BitmapDrawable
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory
import android.view.View
import android.view.animation.Animation
import android.view.animation.Animation.REVERSE
import android.view.animation.TranslateAnimation
import android.widget.ImageView

//region - View
var View.visible: Boolean
    get() = visibility == View.VISIBLE
    set(value) {
        visibility = if (value) View.VISIBLE else View.GONE
    }

var View.invisible: Boolean
    get() = visibility == View.INVISIBLE
    set(value) {
        visibility = if (value) View.INVISIBLE else View.VISIBLE
    }

fun View.shake() {
    val shake: Animation = TranslateAnimation(-20.0f, 20.0f, 0.0f, 0.0f)
    shake.duration = 50L
    shake.repeatCount = 5
    shake.repeatMode = REVERSE
    startAnimation(shake)
}
//endregion

//region - ImageView
fun ImageView.setRounded(imageSize: Int) {
    val bitmap = this.drawable as? BitmapDrawable
    bitmap?.let {
        val image = RoundedBitmapDrawableFactory.create(resources, it.bitmap)
        image.isCircular = true
        image.cornerRadius = imageSize / 2.0f
        this.setImageDrawable(image)
    }
}
//endregion