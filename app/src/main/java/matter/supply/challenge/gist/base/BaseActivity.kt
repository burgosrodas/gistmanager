package matter.supply.challenge.gist.base

import android.support.annotation.StringRes
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.toolbar.*
import matter.supply.challenge.gist.R
import matter.supply.challenge.gist.utils.ShowMessage

abstract class BaseActivity : AppCompatActivity() {

    private var showArrow = false
    var rootView: View? = null

    //region overrides
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun setContentView(view: View?) {
        super.setContentView(view)

        if (view != null) {
            rootView = view
            val toolbar = rootView?.findViewById<Toolbar>(R.id.toolbar)

            toolbar?.let {
                setSupportActionBar(it)
                setBackArrow(this.showArrow)
            }
        }
    }

    override fun setContentView(layoutResID: Int) {
        super.setContentView(layoutResID)
        toolbar?.let {
            setSupportActionBar(it)
            setBackArrow(this.showArrow)
        }
    }
    //endregion

    //region public/protected
    /**
     * Will enable/Disable the back arrow in toolbar.
     *
     * @param showArrow indicates if arrow should be displayed or not.
     */
    fun setBackArrow(showArrow: Boolean) {
        this.showArrow = showArrow
        supportActionBar?.setDisplayHomeAsUpEnabled(this.showArrow)
    }

    /**
     * Will display a dialog message with a text from resources.
     *
     * @param stringId for the text to be displayed
     */
    fun showMessage(@StringRes stringId: Int) {
        showMessage(getString(stringId))
    }

    /**
     * Will display a dialog message with a button and a text.
     *
     * @param message to be displayed.
     */
    fun showMessage(message: String) {
        ShowMessage.Builder(this)
            .setMessage(message)
            .setDialogType(ShowMessage.DialogType.DIALOG)
            .setPositive(R.string.default_yes)
            .setAction(object : ShowMessage.DialogConfirmInterface {
                override fun onButtonClicked(buttonClicked: ShowMessage.ButtonType) {}
            })
            .show()
    }
    //endregion
}