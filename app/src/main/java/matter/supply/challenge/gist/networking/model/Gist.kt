package matter.supply.challenge.gist.networking.model

import android.os.Parcel
import android.os.Parcelable

data class Gist(
    val comments: Int,
    val comments_url: String,
    val commits_url: String,
    val created_at: String,
    val description: String,
    val forks_url: String,
    val git_pull_url: String,
    val git_push_url: String,
    val html_url: String,
    val id: String,
    val node_id: String,
    val owner: Owner,
    val `public`: Boolean,
    val truncated: Boolean,
    val updated_at: String,
    val url: String,
    val user: String
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readParcelable(Owner::class.java.classLoader),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: ""
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(comments)
        parcel.writeString(comments_url)
        parcel.writeString(commits_url)
        parcel.writeString(created_at)
        parcel.writeString(description)
        parcel.writeString(forks_url)
        parcel.writeString(git_pull_url)
        parcel.writeString(git_push_url)
        parcel.writeString(html_url)
        parcel.writeString(id)
        parcel.writeString(node_id)
        parcel.writeParcelable(owner, flags)
        parcel.writeByte(if (public) 1 else 0)
        parcel.writeByte(if (truncated) 1 else 0)
        parcel.writeString(updated_at)
        parcel.writeString(url)
        parcel.writeString(user)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<Gist> {
        override fun createFromParcel(parcel: Parcel): Gist {
            return Gist(parcel)
        }

        override fun newArray(size: Int): Array<Gist?> {
            return arrayOfNulls(size)
        }
    }
}