package matter.supply.challenge.gist.networking.interfaces

import io.reactivex.Observable
import matter.supply.challenge.gist.networking.model.Gist
import retrofit2.http.GET
import retrofit2.http.Path

interface GistAPI {

    @GET("gists")
    fun loadAllGists(): Observable<List<Gist>>

    @GET("/users/{username}/gists")
    fun loadGistsFromUser(@Path("username") userName: String): Observable<List<Gist>>
}