package matter.supply.challenge.gist.networking

import com.google.gson.GsonBuilder
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import matter.supply.challenge.gist.networking.interfaces.GistAPI
import matter.supply.challenge.gist.networking.model.Gist
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class GistController {

    companion object {
        const val BASE_URL = "https://api.github.com/"
    }

    private val gistApi: GistAPI?

    init {
        val gson = GsonBuilder()
            .setLenient()
            .setDateFormat("yyyy-MM-dd")
            .create()

        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        gistApi = retrofit.create(GistAPI::class.java)
    }

    //region Public
    /**
     * Loads from Github servers the public Gist list.
     *
     * @return an [@link io.reactivex.Observable] for the Github response.
     */
    fun getPublicGistList(): Observable<List<Gist>>? = gistApi?.loadAllGists()
            ?.observeOn(AndroidSchedulers.mainThread())

    /**
     * Loads from Github servers the public Gist list for a given [userName].
     *
     * @param userName to be searched in github's servers.
     *
     * @return an [@link io.reactivex.Observable] for the Github response.
     */
    fun getUserPublicGistList(userName: String): Observable<List<Gist>>? = gistApi?.loadGistsFromUser(userName)
            ?.observeOn(AndroidSchedulers.mainThread())
    //endregion
}