package matter.supply.challenge.gist.base

import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseFragment : Fragment() {

    private val disposables: CompositeDisposable = CompositeDisposable()

    //region Override
    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }
    //endregion

    //region Public
    /**
     * Will display a dialog message with a text from resources.
     *
     * @param stringId for the text to be displayed
     */
    fun showDialog(@StringRes stringId: Int) {
        (activity as? BaseActivity)?.showMessage(stringId)
    }

    /**
     * Will display a dialog message with a button and a text.
     *
     * @param message to be displayed.
     */
    fun showDialog(message: String) {
        (activity as? BaseActivity)?.showMessage(message)
    }

    /**
     * Will add disposables to the composite to be unsubscribed later.
     *
     * @param disposable to be added to the composite.
     */
    fun addDisposable(disposable: Disposable?) {
        disposable?.let {
            disposables.add(it)
        }
    }
    //endregion
}