package matter.supply.challenge.gist.ui.gist.detail

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.text.util.Linkify
import android.view.MenuItem
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.detail_activity.*
import kotlinx.android.synthetic.main.toolbar_collapsable.*
import matter.supply.challenge.gist.R
import matter.supply.challenge.gist.base.BaseActivity
import matter.supply.challenge.gist.utils.visible

class GistDetailActivity : BaseActivity() {

    private lateinit var viewModel: GistDetailViewModel

    //region Override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.detail_activity)
        setBackArrow(true)

        viewModel = ViewModelProviders.of(this).get(GistDetailViewModel::class.java)
        intent?.apply {
            if (hasExtra(Intent.EXTRA_USER)) {
                viewModel.setUserGist(getParcelableExtra(Intent.EXTRA_USER))
                initData()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                supportFinishAfterTransition()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
    //endregion

    //region Private
    private fun initData() {
        viewModel.getUserGist()?.apply {
            title = owner.login

            if (description.isNotEmpty()) {
                detailDescription?.text = description
            } else cardViewComments?.visible = false

            detailURL?.text = owner.url
            detailSubscriptionsURL?.text = owner.subscriptions_url

            Linkify.addLinks(detailURL, Linkify.WEB_URLS)
            Linkify.addLinks(detailSubscriptionsURL, Linkify.WEB_URLS)

            Picasso.get()
                .load(owner.avatar_url)
                .error(R.drawable.ic_launcher_background)
                .into(detailImage)
        }
    }
    //endregion
}