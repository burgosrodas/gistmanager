package matter.supply.challenge.gist.ui.gist.list

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_gist_list.view.*
import matter.supply.challenge.gist.R
import matter.supply.challenge.gist.networking.model.Gist

/**
 * Class to handle the list to be displayed.
 *
 * @param clickListener to report click events over an item.
 */
class GistListAdapter(private val clickListener: (Gist, View) -> Unit) :
    RecyclerView.Adapter<GistListAdapter.ViewHolder>() {

    private var list: ArrayList<Gist> = arrayListOf()

    //region Overrides
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_gist_list, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = list[position]
        holder.bind(data, clickListener)
    }
    //endregion

    //region Public
    /**
     * Will set the items to the adapter.
     *
     * @param newItems new list of items to be assigned to the adapter.
     */
    fun setItems(newItems: List<Gist>) {
        list.clear()
        list.addAll(newItems)
        notifyDataSetChanged()
    }
    //endregion

    class ViewHolder(var view: View) : RecyclerView.ViewHolder(view) {

        /**
         * Will fill the info in the [view].
         *
         * @param data the info retrieved from server.
         * @param clickListener to inform which item was clicked.
         */
        fun bind(data: Gist, clickListener: (Gist, View) -> Unit) {

            view.gistOwnerName.text = data.owner.login
            view.gistDescription.text = data.description
            view.createdDate.text = data.created_at

            Picasso.get()
                .load(data.owner.avatar_url)
                .resize(
                    view.resources.getDimension(R.dimen.owner_image_size).toInt(),
                    view.resources.getDimension(R.dimen.owner_image_size).toInt()
                )
                .placeholder(R.drawable.vector_user_placeholder)
                .error(R.drawable.vector_user_placeholder)
                .into(view.ownerImage)

            view.setOnClickListener {
                clickListener.invoke(data, view)
            }

        }
    }
}


