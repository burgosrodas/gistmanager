package matter.supply.challenge.gist.ui.gist.list

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.util.Pair
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.android.synthetic.main.message_layout.*
import matter.supply.challenge.gist.R
import matter.supply.challenge.gist.base.BaseFragment
import matter.supply.challenge.gist.ui.gist.detail.GistDetailActivity
import matter.supply.challenge.gist.utils.visible
import retrofit2.HttpException

class GistListFragment : BaseFragment() {

    companion object {
        const val TAG = "GistListFragment"
        fun newInstance() = GistListFragment()
    }

    private lateinit var adapter: GistListAdapter
    private lateinit var viewModel: GistListViewModel
    private val adapterListener = object : RecyclerView.AdapterDataObserver() {
        override fun onChanged() {
            super.onChanged()
            if (adapter.itemCount == 0) {
                showEmptyMessage(true)
                progressBar?.visible = false
            } else {
                showEmptyMessage(false)
            }
        }
    }

    //region Overrides
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(GistListViewModel::class.java)
        loadInitData()
    }

    override fun onDestroy() {
        adapter.unregisterAdapterDataObserver(adapterListener)
        super.onDestroy()
    }
    //endregion

    //region Private
    private fun initViews() {
        setProgress(true)
        adapter = GistListAdapter { gist, view ->
            run {
                val intent = Intent(context, GistDetailActivity::class.java)
                intent.putExtra(Intent.EXTRA_USER, gist)
                val p1 = Pair(view.findViewById(R.id.ownerImage) as View, "detailImage")
                val p2 = Pair(view.findViewById(R.id.itemCardViewProductName) as View, "detailTitle")
                val options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity!!, p1, p2)
                startActivity(intent, options.toBundle())
            }
        }
        recyclerListView?.layoutManager = LinearLayoutManager(context)
        recyclerListView?.itemAnimator = DefaultItemAnimator()
        recyclerListView?.adapter = adapter
        adapter.registerAdapterDataObserver(adapterListener)
    }

    private fun setProgress(isLoading: Boolean) {
        progressBar?.visible = isLoading
    }

    private fun showEmptyMessage(showMessage: Boolean) {
        rootMessage?.visible = showMessage
        recyclerListView?.visible = !showMessage
    }

    private fun evaluateError(throwable: Throwable) {
        when (throwable) {
            is HttpException -> showDialog(throwable.response().errorBody()?.string() ?: "")
            else -> showDialog(R.string.error_loading_list_message)
        }
        setProgress(false)
        showEmptyMessage(true)
    }
    //endregion

    //region Public
    /**
     * Will populate the list with default data to display.
     */
    fun loadInitData() {
        setProgress(true)
        addDisposable(viewModel.getPresenter().getPublicGistList()?.subscribe({
            adapter.setItems(it)
        }, {
            Log.e(TAG, "Error loading gist list.", it)
            evaluateError(it)
        }, {
            setProgress(false)
        }))
    }

    /**
     * Will set the adapter with the gist from a given [userName]
     *
     * @param userName query to be searched
     */
    fun searchUser(userName: String) {
        addDisposable(viewModel.getPresenter().getPublicUserGistList(userName)?.subscribe({
            adapter.setItems(it)
        }, {
            Log.e(TAG, "Error loading gist user list for $userName.", it)
            evaluateError(it)
        }, {
            setProgress(false)
        }))
    }
    //endregion
}
