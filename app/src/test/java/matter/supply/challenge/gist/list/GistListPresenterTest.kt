package matter.supply.challenge.gist.list

import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.observers.TestObserver
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.plugins.RxJavaPlugins.setIoSchedulerHandler
import io.reactivex.schedulers.Schedulers
import matter.supply.challenge.gist.networking.model.Gist
import matter.supply.challenge.gist.ui.gist.list.GistListPresenter
import org.junit.After
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import retrofit2.HttpException
import java.util.concurrent.Callable

class GistListPresenterTest {

    private val presenter = GistListPresenter()

    @Before
    fun setUp() {
        RxAndroidPlugins.reset()
        RxJavaPlugins.reset()
        setIoSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(object : Function<Scheduler>,
            io.reactivex.functions.Function<Callable<Scheduler>, Scheduler> {
            override fun apply(t: Callable<Scheduler>): Scheduler {
                return Schedulers.trampoline()
            }
        })
    }

    @After
    fun after() {
        RxAndroidPlugins.reset()
        RxJavaPlugins.reset()
    }

    @Test
    @Throws(HttpException::class)
    fun testLoadInitialList() {
        val testObserver = TestObserver<List<Gist>?>()
        presenter.getPublicGistList()?.subscribe(testObserver)

        testObserver.await()
        testObserver.assertSubscribed()
        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
    }

    @Test
    @Throws(HttpException::class)
    fun testLoadUserGistsListWithUser_returnTrue() {
        val testObserver = TestObserver<List<Gist>?>()
        presenter.getPublicUserGistList("niun")?.subscribe(testObserver)

        testObserver.await()
        testObserver.assertSubscribed()
        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
    }

    @Test
    fun testLoadUserGistsListWithEmptyUserName_returnNotFound() {
        val testObserver = TestObserver<List<Gist>?>()
        presenter.getPublicUserGistList("")?.subscribe(testObserver)

        testObserver.assertSubscribed()
        testObserver.assertNoValues()
        testObserver.assertEmpty()
        testObserver.await()
        testObserver.assertNotComplete()
        assertTrue(testObserver.errors().isNotEmpty())
        testObserver.assertError(HttpException::class.java)
    }
}