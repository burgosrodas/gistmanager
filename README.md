# Gist Manager

Android Project that includes following features:

  - List of public Gists thanks to Github API.
  - Search functionality by username (not filtering the current list)
  - Load images from user profile
  - Selectable items to see a brief description.
  - UnitTest for presenter class, using reactiveX testing.

### Used Libraries!

  - 100% Kotlin
  - Retrofit, Gson for REST connections.
  - RxJava and RxAndroid integrated with Retrofit.
  - Picasso to handle the download images process.
  - Architectural ViewModel class.

Bitbucket Pipelines integrated (CI/CD service):
  - Pipeline that creates the Android APK and automatically upload it to Downloads folder.
  - Execution of **GistListPresenterTest.kt** with each commit.
  - See **bitbucket-pipelines.yml** to see the configuration.

> Free accounts has a restriction with only 50 free minutes that would be consumed with each commit in this development.
> For that reason the **bitbucket-pipelines.yml** has been updated to be executed manually.

### Missing functionality
* **Login Authentication:** in order to create, update or delete Gists.
* **Internet Validation:** The app doesn't make any validation for connection.
* **Orientation:** Just Portrait orientation.
* **UI Testing:** Just Unit Testing for a single class.
* **GPS:** Location wasn't implemented.
* **Gist Detail:** The detail view is not complete, just a couple of links were displayed. 
 *I considered that display more info will be the same result, and I need more time ;)*
* **Documentation:** Just Protected/Public methods were documented.

### Things that I'd like to have
* [Dagger](https://google.github.io/dagger/android.html)
* [Clean Architecture](https://medium.com/exploring-android/learn-clean-architecture-for-android-at-caster-io-8f1513621c30)
* [Espresso](https://developer.android.com/training/testing/espresso/)
* [Fully Architectural components (LiveData, ViewModel)](https://developer.android.com/topic/libraries/architecture/)

### Questions
1. What do you think you would do different if you had 2 weeks to complete
this assignment and no requirement to use Github. What would your
backend solution look like? 
    * I believe that a Solution like [Firebase](https://firebase.google.com/), [Parse](https://parseplatform.org/) it would be helpful with just 2 weeks
    to develop an Android app, almost the full 2 weeks would be to focus in the android solution (or IOS/Web), and do not spend time with the backend solution, besides they already include Crashlytics, Analytics, Logins, OffLine mode for Databases etc...
    

2. How and where do you feel like this application should be deployed/distributed?
    * It depends on the nature of the app, I think that an app like this, it would be helpful for a lot of people, I would consider to distribute the app in the Google Play Store

3. Are you happy with your own solutions? If yes, what parts do you think are
really well done, if not, what would you want to change?
    * I'm Happy with my solution, even though is not complete, Ive managed to list and search info in just a couple of days (mostly working at nights after work)
    The best part was create the retrofit connections (even though urls and constants are not in a resource file) and use the common Scheduler for all requests, and of course, use Observables as responses from Github!